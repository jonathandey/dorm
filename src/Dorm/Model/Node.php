<?php namespace Dorm\Model;

class Node extends \Illuminate\Database\Eloquent\Model {
	protected $primaryKey = 'nid';
	public $timestamps = false;
	protected $table = 'node';

	public function body()
	{
		return $this->morphOne('Dorm\Model\FieldDataBody', 'entity')
		->orWhere('entity_type', 'node');
	}
}