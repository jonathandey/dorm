<?php namespace Dorm\Model;

class FieldDataBody extends \Illuminate\Database\Eloquent\Model {
	protected $primaryKey = null;
	protected $table = 'field_data_body';
	public $incrementing = false;

	public function entity()
	{
		return $this->morphTo();
	}
}