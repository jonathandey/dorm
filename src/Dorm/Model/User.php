<?php namespace Dorm\Model;

class User extends \Illuminate\Database\Eloquent\Model {
	protected $primaryKey = 'uid';
	public $timestamps = false;
}