dorm is an ActiveRecord style ORM, expressive query and schema builder for Drupal 7, thanks to Laravel's illuminate/database reposiroty.

dorm brings the Eloquent functionality of Laravel to Drupal 7. Thank god.

1. To install dorm download illuminate/database (https://github.com/Illuminate/Database) and add it to your libraries folder.
2. Run `composer install` on the downloaded 'database' directory. 
3. Install and enable dorm on your Drupal installation.

dorm requires xautoload.